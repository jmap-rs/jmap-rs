// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use thiserror::Error;

#[derive(Error, Debug)]
pub enum DataError {
    #[error("The id `{id}` is invalid. Reason: {why_invalid}")]
    InvalidId { id: String, why_invalid: String },
    // #[error("The email address `{0}` is invalid.")]
    // InvalidEmail(String),
    #[error(transparent)]
    SerdeJsonError(#[from] serde_json::Error),
}
