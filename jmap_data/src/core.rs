// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use crate::error::DataError;
use lazy_static::lazy_static;
use rand::{distributions::Alphanumeric, thread_rng, Rng};
use regex::Regex;
use serde::{
    de::{Error, SeqAccess, Visitor},
    ser::SerializeTuple,
    Deserialize, Deserializer, Serialize, Serializer,
};
use serde_json::{Map, Value};
use std::{collections::HashMap, fmt, marker::PhantomData, ops::Deref, str::FromStr};
use uritemplate::{IntoTemplateVar, TemplateVar};

lazy_static! {
    static ref VALID_ID_CHARS: Regex = Regex::new(r"^[A-Za-z0-9\-_]*$").unwrap();
}

/// Newtype around String to make sure it is a valid JMAP id.
#[derive(Serialize, Debug, PartialEq, Eq, Hash, Clone)]
pub struct Id(String);

impl Id {
    pub fn inner(&self) -> &String {
        &self.0
    }
}

impl IntoTemplateVar for Id {
    fn into_template_var(self) -> TemplateVar {
        TemplateVar::Scalar(self.0)
    }
}

impl<'de> Deserialize<'de> for Id {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        s.parse().map_err(D::Error::custom)
    }
}

impl FromStr for Id {
    type Err = DataError;
    fn from_str(src: &str) -> Result<Id, DataError> {
        let src = src.to_owned();
        if src.is_empty() || src.len() > 255 {
            return Err(DataError::InvalidId {
                id: src.to_string(),
                why_invalid: format!(
                    "condition not met: 256 > id_length > 0 for length {}",
                    src.len()
                ),
            });
        } else if !VALID_ID_CHARS.is_match(&src) {
            return Err(DataError::InvalidId {
                id: src,
                why_invalid: "disallowed characters. only [A-Za-z0-9], -, and _ are allowed."
                    .to_string(),
            });
        }
        Ok(Id(src))
    }
}

impl Into<String> for Id {
    fn into(self) -> String {
        self.0
    }
}

impl Deref for Id {
    type Target = String;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct SessionResource {
    pub capabilities: HashMap<String, Value>,
    pub accounts: HashMap<Id, Account>,
    pub primary_accounts: HashMap<String, Id>,
    pub username: String,
    pub api_url: String,
    pub download_url: String,
    pub upload_url: String,
    pub event_source_url: String,
    pub state: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Capabilities {
    #[serde(rename = "urn:ietf:params:jmap:core")]
    pub core_params: JmapCoreParams,
    #[serde(rename = "urn:ietf:params:jmap:websocket")]
    pub websocket: Option<JmapWebSocketParams>,
    #[serde(flatten)]
    pub other: Map<String, Value>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Account {
    pub name: String,
    pub is_personal: bool,
    pub is_read_only: bool,
    pub account_capabilities: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct JmapCoreParams {
    pub max_size_upload: u64,
    pub max_concurrent_upload: u64,
    pub max_size_request: u64,
    pub max_concurrent_requests: u64,
    pub max_calls_in_request: u64,
    pub max_objects_in_get: u64,
    pub max_objects_in_set: u64,
    pub collation_algorithms: Vec<String>,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct JmapWebSocketParams {
    pub url: String,
    pub supports_push: bool,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct JmapRequest {
    pub using: Vec<String>,
    pub method_calls: Vec<Invocation>,
    #[serde(default)]
    pub created_ids: HashMap<Id, Id>,
    #[serde(flatten)]
    pub extra: Map<String, Value>,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct JmapResponse {
    pub method_responses: Vec<Invocation>,
    #[serde(default)]
    pub created_ids: HashMap<Id, Id>,
    pub session_state: String,
    #[serde(flatten)]
    pub extra: Map<String, Value>,
}

#[derive(Debug)]
pub struct Invocation {
    pub name: String,
    pub arguments: Map<String, Value>,
    pub method_call_id: String,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ResultReference {
    pub result_of: String,
    pub name: String,
    pub path: String,
}

impl Invocation {
    /// Creates a new Invocation.
    ///
    /// This function should not be used outside of an invocation builder for specific requests.
    ///
    /// # Arguments
    ///
    /// * `name` - A string that holds the name of the method to call or the response
    /// * `arguments` - A map containing the arguments of the call or response
    ///
    ///
    fn new(name: String, arguments: Map<String, Value>) -> Self {
        let method_call_id: String = thread_rng()
            .sample_iter(&Alphanumeric)
            .take(10)
            .map(char::from)
            .collect();
        Invocation {
            name,
            arguments,
            method_call_id,
        }
    }

    fn reference(&self, path: String) -> ResultReference {
        ResultReference {
            result_of: self.method_call_id.clone(),
            name: self.name.clone(),
            path,
        }
    }
}

impl Serialize for Invocation {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut tuple = serializer.serialize_tuple(3)?;
        tuple.serialize_element(&self.name)?;
        tuple.serialize_element(&self.arguments)?;
        tuple.serialize_element(&self.method_call_id)?;
        tuple.end()
    }
}

impl<'de> Deserialize<'de> for Invocation {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct __Visitor<'de> {
            lifetime: PhantomData<&'de ()>,
        }
        impl<'de> Visitor<'de> for __Visitor<'de> {
            type Value = Invocation;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("A three-tuple of String, String[*], String")
            }

            fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
            where
                A: SeqAccess<'de>,
            {
                let name = match seq.next_element()? {
                    Some(name) => name,
                    None => return Err(Error::invalid_length(0usize, &self)),
                };
                let arguments = match seq.next_element()? {
                    Some(args) => args,
                    None => return Err(Error::invalid_length(1usize, &self)),
                };
                let method_call_id = match seq.next_element()? {
                    Some(mci) => mci,
                    None => return Err(Error::invalid_length(2usize, &self)),
                };
                Ok(Invocation {
                    name,
                    arguments,
                    method_call_id,
                })
            }
        }
        deserializer.deserialize_tuple(
            3,
            __Visitor {
                lifetime: PhantomData,
            },
        )
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ProblemDetails {
    #[serde(rename = "type")]
    pub typ: String,
    pub title: Option<String>,
    pub status: Option<u32>,
    pub detail: Option<String>,
    pub instance: Option<String>,
    #[serde(flatten)]
    pub extra: Map<String, Value>,
}

#[derive(Deserialize, Debug)]
#[serde(untagged)]
pub enum ErrorOrResponse<T> {
    Response(T),
    Err(ProblemDetails),
}

pub trait JmapObject {
    fn type_name() -> String;
    fn required_capabilities() -> Vec<String>;
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct BlobUploadResult {
    pub id: Id,
    pub account_id: Id,
    #[serde(rename = "type")]
    pub typ: String,
    pub size: u64,
}
