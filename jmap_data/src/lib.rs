// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#![warn(clippy::all)]
#![allow(dead_code)] // this should be disabled before release
// #[warn(missing_docs)]
pub mod core;
pub mod error;
