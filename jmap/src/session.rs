// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use crate::error::{JmapError, JmapResult};
use jmap_data::core::{BlobUploadResult, ErrorOrResponse, Id, SessionResource};
use mime::Mime;
use reqwest::{Body, Client, StatusCode};
use rustyknife::types::{DomainPart, Mailbox as EmailAddress};
use std::{collections::HashMap, convert::TryFrom, sync::Arc};
use tokio::sync::{Mutex, RwLock};
use trust_dns_resolver::{
    error::{ResolveError, ResolveErrorKind},
    lookup::SrvLookup,
    proto::DnsHandle,
    AsyncResolver, ConnectionProvider,
};
use uritemplate::UriTemplate;
use url::Url;

static LIB_USER_AGENT: &str = concat!("jmap.rs/", env!("CARGO_PKG_VERSION"));

fn no_records_as_none(
    lookup: Result<SrvLookup, ResolveError>,
) -> Result<Option<SrvLookup>, ResolveError> {
    match lookup {
        Ok(srv) => Ok(Some(srv)),
        Err(err) => match err.kind() {
            ResolveErrorKind::NoRecordsFound { .. } => Ok(None),
            _ => Err(err),
        },
    }
}

/// Try to automatically find the session resource for a given domain, using the given DNS resolver
pub async fn get_session_resource<S, C, P>(domain: S, res: AsyncResolver<C, P>) -> JmapResult<Url>
where
    S: Into<String>,
    C: DnsHandle<Error = ResolveError>,
    P: ConnectionProvider<Conn = C>,
{
    let domain: String = domain.into();
    let domain = if domain.ends_with('.') {
        domain
    } else {
        domain + "."
    };
    let domain = format!("_jmap._tcp.{}", domain);
    let session_resource_url: Url = no_records_as_none(res.srv_lookup(domain.as_str()).await)?
        .and_then(|srv_lookup| {
            srv_lookup.iter().next().map(|srv| {
                Url::parse(&format!(
                    "https://{}:{}/.well-known/jmap",
                    srv.target().to_ascii(),
                    srv.port()
                ))
            })
        })
        .unwrap_or_else(|| Url::parse(&format!("https://{}/.well-known/jmap", domain)))?;
    Ok(session_resource_url)
}

pub struct Session {
    session_resource: SessionResource,
    http_client: Client,
    login_email: EmailAddress,
    state: RwLock<HashMap<String, Arc<Mutex<String>>>>,
    capabilities: Vec<String>,
}

pub enum Authentication {
    Basic { username: String, password: String },
}

impl Session {
    /// Tries to initiate a new JMAP session.
    pub async fn try_new<C, P>(
        email_address: String,
        authentication: Authentication,
        session_resource_url: Option<String>,
        res: AsyncResolver<C, P>,
    ) -> JmapResult<Self>
    where
        C: DnsHandle<Error = ResolveError>,
        P: ConnectionProvider<Conn = C>,
    {
        let login_email: EmailAddress = email_address
            .parse()
            .map_err(|_| JmapError::InvalidEmail(email_address))?;
        let session_resource_url = if let Some(sru) = session_resource_url {
            Url::parse(&sru)?
        } else {
            if let DomainPart::Domain(d) = login_email.domain_part() {
                get_session_resource(d.clone(), res).await?
            } else {
                return Err(JmapError::ConnectToIP);
            }
        };
        use reqwest::header;
        let mut headers = header::HeaderMap::new(); //TODO: Authorization, sorta started?
        match authentication {
            Authentication::Basic { username, password } => {
                let auth_header_value = format!(
                    "Basic {}",
                    base64::encode(format!("{}:{}", username, password))
                );
                let auth_header = header::HeaderValue::try_from(auth_header_value)?;
                headers.insert(header::AUTHORIZATION, auth_header);
            }
        };
        let http_client = Client::builder()
            .user_agent(LIB_USER_AGENT)
            .default_headers(headers)
            .build()?;
        let resp = http_client
            .get(session_resource_url.as_str())
            .send()
            .await?;
        match resp.status() {
            StatusCode::OK => {}
            StatusCode::UNAUTHORIZED => return Err(JmapError::BadCredentials(resp)),
            _ => return Err(JmapError::OtherRespError(resp)),
        }
        let session_resource: SessionResource = resp.json().await?;
        Ok(Session {
            session_resource,
            http_client,
            login_email,
            state: RwLock::new(HashMap::new()),
            capabilities: Vec::new(),
        })
    }
    pub fn session_resource(&self) -> &SessionResource {
        &self.session_resource
    }
    pub fn capabilities(&self) -> &Vec<String> {
        &self.capabilities
    }
    pub async fn upload_blob<T>(
        &self,
        acct: Id,
        content_type: Mime,
        blob: T,
    ) -> JmapResult<BlobUploadResult>
    where
        T: Into<Body>,
    {
        let uri = UriTemplate::new(&self.session_resource.upload_url)
            .set("accountId", acct)
            .build();
        let resp = self
            .http_client
            .post(&uri)
            .header(reqwest::header::CONTENT_TYPE, content_type.essence_str())
            .body(blob)
            .send()
            .await?
            .json::<ErrorOrResponse<BlobUploadResult>>()
            .await?;
        match resp {
            ErrorOrResponse::Err(_err) => todo!(),
            ErrorOrResponse::Response(resp) => Ok(resp),
        }
    }
}
