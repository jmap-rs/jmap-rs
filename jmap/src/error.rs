// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use jmap_data::error::DataError;
use reqwest::Response;
use thiserror::Error;
use trust_dns_resolver::error::ResolveError;

/// The Error type returned by all JMAP functions.
#[derive(Error, Debug)]
pub enum JmapError {
    /// An error originating from the jmap_data crate.
    #[error(transparent)]
    Data(#[from] DataError),
    /// An error resolving a DNS record.
    #[error(transparent)]
    Resolve(#[from] ResolveError),
    /// An error while parsing a URL
    #[error(transparent)]
    UrlParse(#[from] url::ParseError),
    /// An error when trying to execute an HTTP request
    #[error(transparent)]
    HttpError(#[from] reqwest::Error),
    /// An error when given an invalid header value.
    #[error(transparent)]
    InvalidHeaderValue(#[from] reqwest::header::InvalidHeaderValue),
    /// An error in JSON ser/de.
    #[error(transparent)]
    JsonError(#[from] serde_json::Error),
    /// An error when the given credentials were not accepted.
    #[error("The given credentials were not accepted.")]
    BadCredentials(Response),
    /// An error returned when an HTTP request fails with an error we don't understand.
    #[error("An unidentified error has occurred.")]
    OtherRespError(Response),
    #[error("The given email address is invalid: {0}")]
    InvalidEmail(String),
    #[error("jmap.rs cannot connect to servers that do not have a domain.")]
    ConnectToIP,
}

/// Alias for Result<T, E> using JmapError.
pub type JmapResult<T> = Result<T, JmapError>;
