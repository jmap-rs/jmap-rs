# jmaplib.rs roadmap
## Invocations
Due to the unique nature of JMAP, we've settled on the following method:  
* Each kind of invocation will have a builder type
* The builder internally stores a map of argument name to value
* Arguments are added with methods on the builder
* If the user wishes to reference another invocation in the request,
a function can be called with that invocation, the argument name,
and the JSON path in that invocation.
* For practical reasons, references WILL NOT be checked on the client side.
The server will return an error if the reference is invalid, which this library
will express as best as possible.